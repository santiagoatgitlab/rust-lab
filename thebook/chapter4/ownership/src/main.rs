fn main() {
    let s = String::from("una cuestion a pantalla");
    let t = take_ownership(s);
    println!("esa variable, te acordás? {}", t);
    borrow();
    let u = dangle();
}

fn take_ownership(texto: String) -> String{
    println!("texto es: {}", texto);
    texto
}

fn borrow(){
    let mut a = "cinco";
    let b = &mut a;
    *b = "seis";
    println!("{}", a);
}

fn dangle() -> &String{
    let s = String::from("lady");
    &s
}
