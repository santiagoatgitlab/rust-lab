fn main() {
    
    let coin = Coin::Quarter(UsState::Nevada);

    let coin_value = match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(us_state) => {
            println!("The state is: {:?}", us_state);
            25
        } 
    };

    println!("The value of coin is: {}", coin_value);

    let five    = Some(5);
    let six     = plus_one(five);
    let none    = plus_one(None);

    let some_u8_value = 7u8;

    match some_u8_value {
        1 => println!("one"),
        3 => println!("three"),
        5 => println!("five"),
        7 => println!("seven"),
        _ => ()
    };

}

#[derive(Debug)]
enum UsState{
    Alabama,
    Alaska,
    Wasington,
    Oregon,
    California,
    Montana,
    Idaho,
    Nevada,
    Utah,
    Arizona
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState)
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None    => None,
        Some(i) => Some(i + 1)
    }
}
