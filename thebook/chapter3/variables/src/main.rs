fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

    
    let x = 5;
    let x = x + 1;
    let x = x * 2;
    println!("The value of x is: {}", x);

    let mut f = 98_222;
    f = f -4;
    println!("The value of f is: {}", f);

    let mut i: u8 = 150;
    i = i * 2;
    println!("The value of i is: {}", i);
}
