fn main() {

    // float

    let x       = 2.0; // f64
    let y: f32  = 3.0; // f32

    println!("chars: {} {}" , x, y); 

    // operations 

    let sum         = 5 + 10;
    let difference  = 95.5 - 4.3;
    let product     = 4 * 30;
    let quotient    = 56.7 / 32.2;
    let remainder   = 43 % 5;

    println!("chars: {} {} {} {} {}" , sum, difference, product, quotient, remainder); 

    // chars

    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    println!("chars: {} {} {}" , c, z, heart_eyed_cat); 

    // compound types

    let tup : (i32, f64, u8)    = (500, 6.4, 1);
    let (x, y, z)               = tup;

    println!("destructuring: {} {} {}" , x, y, z); 
    println!("period operator: {} {} {}", tup.0, tup.1, tup.2);

    let a = [1, 2, 3, 4, 5];

    let months = ["January", "February", "March", "April", "May", "June", "July",
                  "August", "September", "October", "November", "December"];

    let type_determined : [i32; 5] = [1, 2, 3, 4, 5];

    let same : [3; 5];

    println!("month: {}", months[7]);
}
