fn main() {
    let number = 8;

    if number < 5 {
        println!("condition was true");
    }
    else{
        println!("condition was false");
    }

    let number = 12;

    if number % 4 == 0{
        println!("number is divisible by four!");
    } else if number % 3 == 0{
        println!("number is divisible by three!");
    } else if number % 2 == 0{
        println!("number is divisible by two!");
    } else{
        println!("number is not divisible by four, three or two!");
    }

    let condition = false;

    let number = if condition {
        5
    }
    else {
        6
    };
    println!("The value number is: {}", number);
}
