fn main() {
    println!("Hello world");
    another_function();
    f_with_a(5, 6);
    /*try_assign();*/
    /*double_assignment();*/
    expression();
    let x = five();
    println!("the value of x is {}", x);
    let y = plus_one(8);
    println!("plus_one result is {}", y);
}

fn another_function(){
    println!("another function");
}

fn f_with_a(x : i32, y : i32){
    println!("the value of x is {}", x);
    println!("the value of y is {}", y);
}

fn try_assign(){
    // let x = (let y = 6); // not possible!
}

fn double_assignment(){
    let mut x = 5;
    let mut y = 6;
    // x = y = 7; // not possible!
}

fn expression(){
    let x = 5;

    let y = {
        let x = 3;
        x + 1
    };

    println!("the value of y is {}", y);
}

fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32{
    x + 1
}
