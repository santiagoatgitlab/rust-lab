use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use actix_web::get;

async fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world")
}

async fn index2() -> impl Responder {
    HttpResponse::Ok().body("Hello world again")
}

#[get("/hello")]
async fn index3() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let port = 8088;
    println!("Starting server on port: {}", port);
    HttpServer::new( || {
        App::new()
            .route("/", web::get().to(index))
            .route("/again", web::get().to(index2))
            .service(index3)
    })
    .bind(format!("127.0.0.1:{}", port))?
    .run()
    .await
}
