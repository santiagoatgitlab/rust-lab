use std::collections::HashMap;

fn main() {

    let s = String::from("wanda");

    let s2 = String::from("nara");

    let mut map = HashMap::new();
    map.insert(&s, &s2);

    println!("s1 is: {}", s);
    println!("s2 is: {}", s2);

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    scores.entry(String::from("Yellow")).or_insert(40);
    scores.entry(String::from("Red")).or_insert(80);

    for (key,value) in &scores {
        println!("{}: {}", key, value);
    }

    let text = "Hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count +=1;
    }

    let key = "choice";

    let value = map.get(key);
    match value {
        Option::Some(i) => println!("{} value: {}", key, i),
        Option::None => println!("{} is empty!", key)
    }

    if let Some(1) = value {
        println!("value is 1");
    }

    println!("{:?}", map);

}
