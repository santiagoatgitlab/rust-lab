use std::fs::File;
use std::io::ErrorKind;
use std::io;
use std::io::Read;

fn main() {

    fileopen();
}

fn fileopen(){
    let f = File::open("Hello.txt");
    let f = match f {
        Ok(h)   => h,
        Err(e)  => match e.kind() { 
            ErrorKind::NotFound => match File::create("hello.txt"){
                Ok(h)       => h,
                Err(error)  => panic!("Problem creating the file: {:?}", e)
            },
            other_error => panic!("Problem opening the file: {:?}", other_error)
        }
    };
}

fn do_panic(){
    let v = vec![1, 2, 3];
    v[99];
}

fn read_username_from_file() -> Result<String, io::Error> {
    
    let f = File::open("user.txt");

    let mut f = match f {
        Ok(handler) => handler,
        Err(error)  => return Err(error)
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_)       => Ok(s),
        Err(error)  => Err(error)
    }

}

fn read_username_from_file_the_short_way () -> Result<String, io::Error>{

    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)

}
