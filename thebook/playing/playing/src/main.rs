fn main() {

    wrapping();

}

fn numbers(){

    let n: i32 = 12;
    let mut m = 41;
    m = m + n;

    println!("the number is: {}", m);

}

fn wrapping () -> Result<u32, std::num::ParseIntError> {

    let a       = "Mario 89";
    let b       = a.parse::<u32>();
    let b = match b {
        Ok(val)     => val,
        Err(error)  => {
            println!("el error es {}", error);
            return Err(error)
        }
    };
    println!("the b is {}", b);
    Ok(b)
}
