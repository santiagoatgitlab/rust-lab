use std::thread;
use std::time::Duration;


fn simulated_expensive_calculation(intensity: i32) -> i32{
    thread::sleep(Duration::from_secs(2));
    intensity
}

fn main(){
    let simulated_user_specified_value  = 10;
    let simulated_random_value          = 7;

    generate_workout(
        simulated_user_specified_value,
        simulated_random_value
    );
}

fn generate_workout(intensity: i32, random_number: i32){

    let expensive_closure = |num| {
        println!("Calculating slowly...");
        thread::sleep(Duration::from_secs(2));
        num
    };

    let expensive_result = simulated_expensive_calculation(intensity);

    if intensity < 25 {
        println!(
            "Today, do {} pushups!",
            expensive_closure(intensity)
        );
        println!(
            "Next, do {} situps!",
            expensive_closure(intensity)
        );
    } else {
        if random_number == 3 {
            println!("Take a break today! remember to stay hidrated!");
        } else {
            println!(
                "Today, run for {} minutes!",
                expensive_closure(intensity)
            );
        }
    }
}
