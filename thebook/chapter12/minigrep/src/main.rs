use std::env;
use std::process;

use minigrep::Config;

fn main() {

    let config = match Config::new(env_args()) {
        Ok(c)   => c,
        Err(m)  => {
            eprintln!("Problem parsing arguments: {}", m);
            process::exit(1);
        }
    };

    if let Err(m) = minigrep::run(config) {
        eprintln!("Error in application: {}", m);
        process::exit(1);
    }

}
