use std::fs;
use std::env;
use std::error::Error;

pub struct Config {
    pub query           : String,
    pub filename        : String,
    pub case_sensitive  : bool
}

impl Config {
    pub fn new (mut args: std::env::Args) -> Result<Config, &'static str> {

        args.next();

        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string")
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name")
        };

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(
            Config {
                query           : args[1].clone(),
                filename        : args[2].clone(),
                case_sensitive  : case_sensitive
            }
        )
    }
}

pub fn run (config: Config) -> Result<(), Box<dyn Error>>{
    let contents = fs::read_to_string(config.filename)?;

    let results = if config.case_sensitive {
        search(&config.query,&contents)
    }
    else {
        search_case_insensitive(&config.query,&contents)
    };

    for line in results{
        println!("{}", line);
    }

    Ok(())
}

pub fn search<'a> (query: &str, contents: &'a str) ->Vec<&'a str>{
    contents.lines()
        .filter(|line| line.contains(query))
        .collect()
}

pub fn search_case_insensitive<'a> (query: &str, contents: &'a str) ->Vec<&'a str>{
    let query = query.to_lowercase();
    let mut results = Vec::new();
    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }
    results
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_new_returns_query_and_filename(){
        let query       = String::from("anyquery");
        let filename    = String::from("anyfilename.txt");

        let vec = vec![String::from(""),query.clone(),filename.clone()];

        let config = Config::new(&vec).unwrap_or_else(|err| {
            panic!("error generating config struct {}", err);
        });

        assert_eq!(config.query, query);
        assert_eq!(config.filename, filename);
    }

    #[test]
    fn case_sensitive(){
        let query       = "duct";
        let contents    = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape";
        
        assert_eq!(
            vec!["safe, fast, productive."],
            search(query, contents)
        )
    }

    #[test]
    fn case_insensitive(){
        let query = "brand";
        let contents = "\
All We Know Is Falling
Riot
Brand New Eyes
Paramore
After laughter
";
        assert_eq!( vec!["Brand New Eyes"], search_case_insensitive(query, contents));
    }
}
