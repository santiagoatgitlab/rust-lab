fn main() {
   let width1   = 30;
   let height1  = 50;

    println!(
        "The area of the rectangle is {} square pixels",
        area(width1, height1)
    );

    let rect1 = (30,40);

    println!(
        "The area of the rectangle 1 is {} square pixels",
        area_t(rect1)
    );

    let rect2 = Rectangle {
        width   : 40,
        height  : 60
    };

    println!(
        "The area of the rectangle 2 is {} square pixels",
        area_s(&rect2)
    );

    println!(
        "Again, the area of the rectangle 2 is {} square pixels",
        rect2.area()
    );

    println!("Rectangle 2 is: {:#?}", rect2);

    let rect3 = Rectangle {
        width   : 20,
        height  : 50
    };

    let rect4 = Rectangle {
        width   : 100,
        height  : 40
    };

    let square = Rectangle::square(30);

    println!("can rect3 fit inside rect2? {}", rect2.can_hold(&rect3));
    println!("can rect3 fit inside rect4? {}", rect4.can_hold(&rect3));
    println!("can square fit inside rect4? {}", rect4.can_hold(&square));
}

fn area(width: u32, height: u32) -> u32 {
    let area = width * height;
    area
}

fn area_t(dimansions: (u32, u32)) -> u32 {
    let area = dimansions.0 * dimansions.1;
    area
}

fn area_s(rectangle: &Rectangle) -> u32 {
    let area = rectangle.width * rectangle.height;
    area
}

#[derive(Debug)]
struct Rectangle {
    width   : u32,
    height  : u32
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, rect: &Rectangle) -> bool{
        { 
            rect.width < self.width &&
            rect.height < self.height
        }
    }

    fn square(size: u32) -> Rectangle {
        Rectangle { width : size, height : size }
    }
}
