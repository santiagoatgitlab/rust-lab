fn main() {

    let santi = User {
        username        : String::from("santi1985"),
        email           : String::from("santiago@gmail.com"),
        sign_in_count   : 0,
        active          : true
    };

    println!("username is: {}", santi.username);

    let ricky = User {
        username        : String::from("ricky1991"),
        email           : String::from("ricardo@gmail.com"),
        ..santi
    };
    
    println!("username is: {}", ricky.username);

    let black   = Color(0, 0, 0);
    let origin  = Point(0, 0, 0);
}

struct User {
    username        : String,
    email           : String,
    sign_in_count   : u64,
    active          : bool,
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

